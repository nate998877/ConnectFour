const gameTable = []
const table = []
let playerState
let gameState = 0

function createGameTable(){
    for(let column = 0; column < 7; column++){
        let col = document.createElement("div")
        table.push(col)
        gameTable.push([])
        attrsetter(col, ["class", "column", "id", "column" + column, "onclick", "clicked(event)", "data-column-num", column])
        document.body.appendChild(col)
        for(let row = 0; row < 6; row++){
            let cell = document.createElement("div")
            attrsetter(cell, ["class", "cell", "id", "cell" + ((column * 6) + row), "data-cell-num", ((column * 6) + row)])
            col.appendChild(cell)
            gameTable[column].push(2)
        }
    }
}

function attrsetter(htmlEle, attributes){
    if(attributes.length%2 != 0){
        console.log("Array of attributes not even, How did this happen?")
    } else {
        for(let i = 0; i < attributes.length; i+=2){
            htmlEle.setAttribute(attributes[i], attributes[i+1])
        }
    }
}

function gameLogic(ploink, ev){
    let column = Array.prototype.slice.call(ev.path[1].getElementsByClassName("cell"))
    for(let i = column.length - 1; i >= 0; i--){
        if(column[i].hasChildNodes() === false) {
            let attributes = attributesLogic()
            let columnnum = ev.path[1].dataset.columnNum
            column[i].appendChild(ploink)
            attrsetter(ploink, attributes)
            gameTable[columnnum][i] = playerState
            winCheck()
            if(gameState == 1){
                setTimeout(function(){return prompt("do you feel good about winning against yourself?") }, 100)
                for(let x of table){
                    x.removeAttribute("onclick")
                }
                setTimeout(function(){location.reload(); }, 3000)
            } else if(gameState == 2){
                setTimeout(function(){return prompt("How bad do you have to be to TIE against yourself?") }, 100)
                for(let x of table){
                    x.removeAttribute("onclick")
                }
                setTimeout(function(){location.reload(); }, 3000)
            }
            break
        } else if(i==0) {
            console.log("ERROR:Index Out Of Bounds")
            if(playerState = 0) { playerState = 1 }
            else { playerState = 0 }
        }
    }
}

function attributesLogic(){
    let attributes
    if(playerState == undefined || playerState == 1){
        playerState = 0
        attributes = ["class", "player1"]
    } else {
        playerState = 1
        attributes = ["class", "player2"]
    }
    return attributes
}




function winCheck(){
    verticalCheck()
    hortizontalCheck()
    negativeSlopeCheck()
    postiveSlopeCheck()
    tieCheck()
}

function verticalCheck(){
    for(let column = 0; column < gameTable.length; column++){
        const strChecker = new String(gameTable[column])
        if(strChecker.includes("0,0,0,0")){
            console.log("player 1 wins with a vertical win")
            gameState = 1
            return
        } else if(strChecker.includes("1,1,1,1")){
            console.log("player 1 wins with a vertical win")
            gameState = 1
            return
        }
    }
}

function hortizontalCheck(){
    for(let row = 0; row < gameTable.length; row++){
        let arrChecker = []
        for(let column = 0; column < gameTable[row].length; column++){
            arrChecker.unshift(gameTable[column][row])
        }

        let strChecker = new String(arrChecker)
        if(strChecker.includes("0,0,0,0")){
            console.log("player 1 wins with a horitzontal win")
            gameState = 1
            return
        } else if(strChecker.includes("1,1,1,1")){
            console.log("player 1 wins with a hortizontal win")
            gameState = 1
            return
        }
    }
}

function negativeSlopeCheck(){
    let row = 0
    for(let column = 0; column < gameTable.length; column++){
        let arrChecker = []
        for(let sloper = 0; column + sloper < gameTable.length; sloper++){
            arrChecker.unshift(gameTable[column+sloper][row+sloper])
        }
        let strChecker = new String(arrChecker)
        if(strChecker.includes("0,0,0,0")){
            console.log("player 1 wins with a n-slope win")
            gameState = 1
            return
        } else if(strChecker.includes("1,1,1,1")){
            console.log("player 1 wins with a n-slope win")
            gameState = 1
            return
        }
    }
    let column = 0
    for(let row = 0; row < gameTable[0].length; row++){
        let arrChecker = []
        for(let sloper = 0; row + sloper < gameTable[0].length; sloper++){
            arrChecker.unshift(gameTable[column+sloper][row+sloper])
        }
        let strChecker = new String(arrChecker)
        if(strChecker.includes("0,0,0,0")){
            console.log("player 1 wins with a p-slope")
            gameState = 1
            return
        } else if(strChecker.includes("1,1,1,1")){
            console.log("player 1 wins with a p-slope")
            gameState = 1
            return
        }
    }
}

function postiveSlopeCheck(){
    let row = gameTable[0].length - 1
    for(let column = 0; column < gameTable.length; column++){
        let arrChecker = []
        for(let sloper = 0; column + sloper < gameTable.length; sloper++){
            arrChecker.unshift(gameTable[column+sloper][row-sloper])
        }
        let strChecker = new String(arrChecker)
        if(strChecker.includes("0,0,0,0")){
            console.log("player 1 wins with a p-slope")
            gameState = 1
            return
        } else if(strChecker.includes("1,1,1,1")){
            console.log("player 1 wins with a p-slope")
            gameState = 1
            return
        }
    }
    let column = 0
    for(let row = gameTable[0].length - 1; row > 0; row--){
        let arrChecker = []
        for(let sloper = 0; row + sloper < gameTable[0].length; sloper++){
            arrChecker.unshift(gameTable[column+sloper][row+sloper])
        }
        let strChecker = new String(arrChecker)
        if(strChecker.includes("0,0,0,0")){
            console.log("player 1 wins with a p-slope")
            gameState = 1
            return
        } else if(strChecker.includes("1,1,1,1")){
            console.log("player 1 wins with a p-slope")
            gameState = 1
            return
        }
    }
}

function tieCheck(){
    let arrChecker = []
    for(let column = 0; column < gameTable.length; column++){
        arrChecker.unshift(gameTable[column][0])
    }
    let strChecker = new String(arrChecker) 
    if(!strChecker.includes("2")){
        console.log("Game Tied")
        prompt("Tie Game")
        gameState = 2
        return
    }
}


function clicked(ev){
    let ploink = document.createElement("div")
    gameLogic(ploink, ev)
}

createGameTable()